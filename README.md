# LVIP插件
Leo的VIP插件

## 適用伺服器版本
mcbe 1.5 (應該可以用)

## 使用方法
主要指令：`/v`，會有説明顯示，首次啟動插件會生成config.yml文件，可自行調整各VIP等級的權限

## Copyright (c) 2018 Lee Siu San

[https://gitlab.com/leolee3914](https://gitlab.com/leolee3914)