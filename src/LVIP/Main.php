<?php
namespace LVIP;

use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\level\sound\LaunchSound;
use pocketmine\level\sound\PopSound;
use pocketmine\level\sound\ClickSound;
use pocketmine\level\sound\FizzSound;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\command\Command;
use pocketmine\network\Network;
use pocketmine\command\CommandSender;
use pocketmine\level\Position;
use pocketmine\level\Level;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\entity\Effect;
use pocketmine\Server;
use pocketmine\OfflinePlayer;
use pocketmine\utils\Config;
use pocketmine\command\ConsoleCommandSender;
use pocketmine\math\Vector3;
use pocketmine\scheduler\PluginTask;
use pocketmine\block\Block;
use pocketmine\event\entity\EntityDeathEvent;
use pocketmine\event\entity\ExplosionPrimeEvent;
use pocketmine\entity\Entity;
use pocketmine\event\player\PlayerItemHeldEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\tile\Sign;
use pocketmine\tile\Tile;
use pocketmine\utils\TextFormat as TF;
use pocketmine\event\player\PlayerDeathEvent;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\event\block\SignChangeEvent;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\block\BlockPlaceEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;
use pocketmine\event\player\PlayerRespawnEvent;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\event\inventory\InventoryPickupItemEvent;
use pocketmine\event\player\PlayerDropItemEvent;
use pocketmine\event\inventory\InventoryOpenEvent;
use pocketmine\event\player\PlayerItemConsumeEvent;

class Main extends PluginBase implements Listener{
	
	private static $th = null;
	
	public static function geti () {
		return self::$th;
	}
	
	function onEnable () {
		if ( !self::$th instanceof \LVIP\Main )
			self::$th = $this;
		$this->getServer()->getPluginManager()->registerEvents($this,$this);
		@mkdir ($this->getDataFolder());
		$this -> config = new Config ( $this->getDataFolder() . "config.yml" , Config::YAML , array() );
		if ( ! $this->config->exists('VIP1') ) $this->config->set('VIP1',['n'=>'VIP1',2=>'',3=>'',10=>'',11=>10000]);
		if ( ! $this->config->exists('VIP2') ) $this->config->set('VIP2',['n'=>'VIP2',2=>'',3=>'',4=>'',5=>'',10=>'',11=>20000,12=>'']);
		if ( ! $this->config->exists('VIP3') ) $this->config->set('VIP3',['n'=>'VIP3',2=>'',3=>'',4=>'',5=>'',9=>'',10=>'',11=>30000,12=>'']);
		if ( ! $this->config->exists('VIP4') ) $this->config->set('VIP4',['n'=>'VIP4',2=>'',3=>'',4=>'',5=>'',7=>'',8=>'',9=>'',10=>'',11=>50000,12=>'']);
		if ( ! $this->config->exists('VIP5') ) $this->config->set('VIP5',['n'=>'VIP5',1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'',10=>'',12=>'']);
		if ( ! $this->config->exists('sign') ) $this->config->set('sign', 300 );
		if ( ! $this->config->exists('max') ) $this->config->set('max', 5 );
		$m = $this->config->get('max');
		if ( $m > 9999 or $m < 1 ) {
			$this->config->set('max', 5 );
			$m = 5;
		}
		for ( $i=1;$i<=$m;$i++ ) {
			if ( ! $this->config->exists("VIP$i") ) $this->config->set("VIP$i",['n'=>"VIP$i",1=>'',2=>'',3=>'',4=>'',5=>'']);
		}
		if ( ! $this->config->exists('buy-VIP') ) $this->config->set('buy-VIP', false );
		if ( ! $this->config->exists('buy-1day') ) $this->config->set('buy-1day', 5000 );
		if ( ! $this->config->exists('buy-1hour') ) $this->config->set('buy-1hour', 3500 );
		$this->config -> save () ;
		$this -> vip = new Config ( $this->getDataFolder() . "vip.yml" , Config::YAML , array() );
		$this -> hvip = new Config ( $this->getDataFolder() . "hourvip.yml" , Config::YAML , array() );
		$this -> sign = new Config ( $this->getDataFolder() . "sign.yml" , Config::YAML , array() );
		$this->j = [];
		$this->t = [];
		if ( count($this->vip->getAll()) > 0 ) {
			foreach ( $this->vip->getAll() as $k => $v ) {
				if ( !$k OR !$v ) continue;
				if ( strtotime($this->vip->get($this->ec($k))['et'])<time() ) {
					$this -> delvip ( $k,1 ) ;
				}
			}
		}
		if ( count($this->hvip->getAll()) > 0 ) {
			foreach ( $this->hvip->getAll() as $k => $v ) {
				if ( !$k OR !$v ) continue;
				if ( $this->hvip->get($this->ec($k))['et']<=0 ) {
					$this -> delvip ( $k,2 ) ;
				}
			}
		}
		$this -> qh = array (
			'1'=>'轉換Gamemode',
			'2'=>'飛行',
			'3'=>'時間',
			'4'=>'藥水',
			'5'=>'傳送',
			'6'=>'踢玩家下線',
			'7'=>'殺死玩家',
			'8'=>'Give物品給自己',
			'9'=>'紫色文字(/say)',
			'10'=>'聊天金色名字',
			'11'=>'VIP付款升級',
			'12'=>'設置天氣'
		);
		$this -> cf = array(
			'0'=>'§a-查看我的VIP資料: /vip m',
			'1'=>'§a-轉換Gamemode /vip gm',
			'2'=>'§a-飛行開關: /vip f',
			'3'=>'§a-設置時間: /vip t <時間>',
			'4'=>'§a-藥水效果: /vip e <ID> <時間(秒)> <強度>',
			'5'=>'§a-傳送到其他玩家: /vip tp <玩家名>',
			'6'=>'§a-踢玩家下線: /vip k <玩家名>',
			'7'=>'§a-殺死玩家: /vip kill <玩家名>',
			'8'=>'§a-Give物品給自己: /vip g <物品ID[:0]> <數量>',
			'9'=>'§a-紫色文字通知: /vip s <內容>',
			'11'=>'§a-VIP升級: /vip up [y]',
			'12'=>'§a-設置天氣: /vip w <天氣ID(0晴天1下雨2雷雨)>'
		);
		$this -> opcf = array(
			'add'=>'§e-增加一位VIP: /vip add <玩家名> <類型> <時間(天/小時)> [等級(1-5)]'."\n".'  §b(類型0=天,類型1=小時,類型1只計算上線時間)',
			'del'=>'§e-刪除一位VIP: /vip del <玩家名>',
			'list'=>'§e-查看VIP名單: /vip list',
			'at'=>'§e-增加VIP時間: /vip at <玩家名> <類型> <時間(天/小時)>',
			'dt'=>'§e-減少VIP時間: /vip dt <玩家名> <類型> <時間(天/小時)>',
			'l'=>'§e-設置VIP等級: /vip l <玩家名> <VIP等級>',
			'qi'=>'§e-查看VIP權限ID: /vip qi'
		);
		$this->getScheduler()->scheduleRepeatingTask(new CallbackTask([$this,"viptime"]),1200);
	}
	
	function onDisable () {
		if ( count($this->vip->getAll()) > 0 ) {
			foreach ( $this->vip->getAll() as $k => $v ) {
				if ( !$k OR !$v ) continue;
				if ( strtotime($this->vip->get($this->ec($k))['et'])<time() ) {
					$this -> delvip ( $k,1 ) ;
				}
			}
		}
		if ( count($this->hvip->getAll()) > 0 ) {
			foreach ( $this->hvip->getAll() as $k => $v ) {
				if ( !$k OR !$v ) continue;
				if ( $this->hvip->get($this->ec($k))['et']<=0 ) {
					$this -> delvip ( $k,2 ) ;
				}
			}
		}
	}
	
	//   Command
	
	function onCommand ( CommandSender $sender , Command $command , string $label , array $args): bool{
		if ( count($args) <= 0 ) goto cr;
		if ( $sender->isOp() ) {
			switch ( $args [0] ) {
				case 'qi':
					foreach ( $this->qh as $k=>$v )
						$sender->sendMessage ( '§e'.$k.':'.$v ) ;
					return true;
				case 'add':
				case 'a':
				case 'at':
					if ( !isset($args[3]) ) goto cr;
					if ( (int)$args[3]<=0 ) goto cr;
					if ( (int)$args[2]<0 AND (int)$args[2]>1 ) goto cr;
					if ( !isset($args[4]) ) $args[4] = 1;
					$level = (int) $args[4];
					if ( isset($args[4]) ) {
						if ( $level<1 ) $level = 1;
						if ( $level>$this->config->get('max') ) $level = $this->config->get('max');
					}else
						$level = 1;
					$tp = (int)$args[2];
					if ( ! $this->addtime( $args[1],(int)$args[3], $level , $tp ) ) {
						$sender->sendMessage ('§c[VIP] 無法增加VIP/時間！！原因: 設置錯誤！') ;
						return true;
					}
					$sender->sendMessage ('§a[VIP] 已增加'.$args[1].'的VIP共'.(int)$args[3]. ($tp==0?'天':'小時') ) ;
					return true;
				case 'dt':
					if ( !isset($args[3]) ) goto cr;
					if ( (int)$args[3]<=0 ) goto cr;
					if ( (int)$args[2]<0 AND (int)$args[2]>1 ) goto cr;
					$tp = (int)$args[2];
					if ( !$this->deltime( $args[1],(int)$args[3] ,$tp ) ) {
						$sender->sendMessage ('§c[VIP] 無法刪減時間！！') ;
						return true;
					}
					$sender->sendMessage ('§a[VIP] 已減少'.$args[1].'的VIP共'.$args[3].($tp==0?'天':'小時')) ;
					return true;
				case 'del':
				case 'd':
					if ( !isset($args[1]) ) goto cr;
					$this->delvip($args[1]);
					$sender->sendMessage ( '§a[VIP] 已刪除'.$args[1].'的VIP' ) ;
					return true;
				case 'list':
					$sender->sendMessage ( '§e VIP名 | 等級 | 到期時間' ) ;
					if ( count($this->vip->getAll()) >0 ) {
						$sender->sendMessage('§c -- 類型0 (天) --');
						foreach ( $this->vip->getAll() as $k=>$v )
							$sender->sendMessage ( '§e'. $k .' §c|§e VIP'. $v['l'].' ('.$this->config->get('VIP'.$v['l'])['n'].'§e) §c|§e '. $v['et'] ) ;
					}
					if ( count($this->hvip->getAll()) >0 ) {
						$sender->sendMessage('§c -- 類型1 (小時) --');
						foreach ( $this->hvip->getAll() as $k=>$v )
							$sender->sendMessage ( '§e'. $k .' §c|§e VIP'. $v['l'].' ('.$this->config->get('VIP'.$v['l'])['n'].'§e) §c|§e '. $this->ct($v['et']) ) ;
					}
					return true;
				case 'l':
					if ( !isset($args[2]) ) goto cr;
					if ( $args[2]<1 OR $args[2]>$this->config->get('max') ) goto cr;
					$this->setlevel($args[1],$args[2]);
					$sender->sendMessage ( '§a[VIP] 設置'.$args[1].'的VIP等級為VIP'.$args[2].'('.$this->config->get('VIP'.$args[2])['n'].'§a)' ) ;
					return true;
			}
		}
		if ( $this->isvip($sender) AND $sender instanceof Player ) {
			switch ( $args [0] ) {
				case 'm':
					$p = $this->ec($sender);
					if ( $this->wvip($p) == 'vip' ) {
						$i = $this->vip->get($p);
						$sender->sendMessage ( '§e[VIP] 你的VIP等級是§a '.$this->config->get('VIP'.$i['l'])['n'].' §e,到期日是§b'.$i['et']."\n".'§e[VIP] 於§b'.$i['st'].'§e購買/獲得' ) ;
					} else {
						$i = $this->hvip->get($p);
						$sender->sendMessage ( '§e[VIP] 你的VIP等級是§a '.$this->config->get('VIP'.$i['l'])['n'].' §e,VIP時間尚餘§b'.$this->ct($i['et'])."\n".'§e[VIP] 於§b'.$i['st'].'§e購買/獲得' ) ;
					}
					return true;
				case 'gm':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [1] ) ) goto cr;
					if ( $sender->getGamemode() != 0 ) {
						$sender->setGamemode ( 0 );
						$sender->sendMessage('§a[VIP] 成功轉換生存模式');
					} else {
						$sender->setGamemode ( 1 );
						$sender->sendMessage('§a[VIP] 成功轉換創造模式');
					}
					return true;
				case 'f':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [2] ) ) goto cr;
					$f = $sender->getAllowFlight();
					$sender->setAllowFlight(!$f);
					if($f){
						$sender->sendMessage('§a[VIP] 成功關閉飛行');
					} else {
						$sender->sendMessage('§a[VIP] 成功開啟飛行');
					}
					return true;
				case 't':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [3] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					$sender->getLevel()->setTime( (int) $args[1] );
					$sender->sendMessage ( '§a[VIP] 時間設置至'.(int) $args[1] ) ;
					return true;
				case 'e':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [4] ) ) goto cr;
					if ( !isset($args[3]) ) goto cr;
					$args[2] = ($args[2]<1 OR $args[2]>600) ? 15 : $args[2];
					$e = Effect::getEffect( (int)(($args[1]>=1 AND$args[1]<=23)?$args[1]:1) )->setDuration((int)$args[2]*20)->setAmplifier((int)$args[3]);
					$sender->addEffect( $e );
					$sender->sendMessage ( '§a[VIP] 成功增加藥水效果！' ) ;
					return true;
				case 'tp':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [5] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					$p = $this->getServer()->getPlayer($args[1]);
					if ( $p ) {
						$this->getServer()->getPluginManager()->callEvent( $ev = new VIPtpEvent ( $p ) );
						if ( $ev->isCancelled() ) {
							$sender->sendMessage ( TF::RED.'§c[VIP] 未知原因,無法傳送！！' );
							return true;
						}
						$sender->teleport ( $p->getPosition() ) ;
						$sender->sendMessage ( '§a[VIP] 傳送成功！' ) ;
						$p->sendMessage ( '§e[VIP] !!注意!! VIP: '.$sender->getName().' 傳送至你身旁' ) ;
						return true;
					}
					$sender->sendMessage ( '§e[VIP] 玩家不在線…' ) ;
					return true;
				case 'k':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [6] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					if ( !isset($args[2]) OR !$args[2] ) $args[2] = '';
					$p = $this->getServer()->getPlayer($args[1]);
					if ( !$p ) {
						$sender->sendMessage ( '§e[VIP] 玩家不在線…' ) ;
						return true;
					}
					if ( $p->isOp() OR $this->isvip($p) ) {
						$sender->sendMessage ('§c[VIP] 無法踢走VIP或OP') ;
						return true;
					}
					$p->kick($args[2].'from '.$sender->getName());
					$sender->sendMessage ( '§a[VIP] 成功踢走'.$p->getName().'原因:'.$args[2] ) ;
					return true;
				case 'kill':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [7] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					$p = $this->getServer()->getPlayer($args[1]);
					if ( !$p ) {
						$sender->sendMessage ( '§e[VIP] 玩家不在線…' ) ;
						return true;
					}
					if ( $p->isOp() OR $this->isvip($p) ) {
						$sender->sendMessage ('§c[VIP] 無法殺死VIP或OP') ;
						return true;
					}
					$p->kill();
					$sender->sendMessage ('§a[VIP] 已經殺掉玩家') ;
					return true;
				case 'g':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [8] ) ) goto cr;
					if ( !isset($args[2]) ) goto cr;
					$ii = explode ( ':' , $args[1] ) ;
					$i = Item::get ( (int)($ii[0]) , (int)(( !isset($ii[1])OR empty($ii[1]) ) ? 0 : $ii[1] ) , $args[2] ) ;
					$sender->getInventory()->addItem( $i );
					$sender->sendMessage('§a[VIP] 成功Give物品！');
					return true;
				case 's':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [9] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					unset($args[0]);
					$m = implode ( ' ',$args ) ;
					$this->getServer()->broadcastMessage( '§d['.$sender->getDisplayName().'] '.$m );
					return true;
				case 'w':
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [12] ) ) goto cr;
					if ( !isset($args[1]) ) goto cr;
					(int)$w = (int)$args[1];
					if ( $w < 0 OR $w > 2 OR ! is_numeric($w) ) goto cr;
					$sender -> getLevel () -> getWeather () -> setWeather ( $w ) ;
					$sender -> sendMessage ( '§a[VIP] 成功設置地圖'.$sender->getLevel()->getName().'的天氣！' ) ;
					return true;
				case 'sign':
					if ( $this->config->get('sign')!=false ) {
						if ( !$this->sign->exists( $this->ec($sender).date('Y/n/j') ) ) {
							$this->getServer()->getPluginManager()->getPlugin('EconomyAPI')->addMoney($sender,(int)$this->config->get('sign'));
							$this->sign->set( $this->ec($sender).date('Y/n/j') , '' );
							$this->sign-> save () ;
							$sender->sendMessage ('§a[VIP] VIP每天簽到成功!你已收到錢$'. (int)$this->config->get('sign') ) ;
							return true;
						}
						$sender->sendMessage ('§c[VIP] 請勿重覆使用VIP每天簽到') ;
						return true;
					}
					goto cr;
				case 'up':
					if ($this->checklevel($sender)>=$this->config->get('max')){
						$sender->sendMessage ( '§a[VIP] 你已經滿級' ) ;
						return true;
					}
					if ( ! isset($this->config->get('VIP'.$this->checklevel($sender) ) [11] ) ) goto cr;
					if ( isset($args[1])AND $args[1]=='y' ) {
						$m = $this->getServer()->getPluginManager()->getPlugin("EconomyAPI")->myMoney($sender->getName());
						(int)$tm = $this->config->get('VIP'.$this->checklevel($sender))[11];
						if ( $m < $tm ) {
							$sender->sendMessage ( '§c[VIP] 你的錢不夠！！' ) ;
							return true;
						}
						$wv = $this->wvip ( $sender ) ;
						if ( $wv == 'hvip' ) {
							$this->hvip->setNested ( $this->ec($sender).'.l' , $this->hvip->get($this->ec($sender))['l']+1 ) ;
							$this->hvip->save();
						}
						elseif ( $wv == 'vip' ) {
							$this->vip->setNested ( $this->ec($sender).'.l' , $this->vip->get($this->ec($sender))['l']+1 ) ;
							$this->vip->save();
						}
						$this->getServer()->getPluginManager()->getPlugin("EconomyAPI")->reduceMoney($sender->getName(), $tm );
						$sender->sendMessage ( '§a[VIP] 已經升了一級！！' ) ;
						return true;
					}
					$sender->sendMessage ( '§b[VIP] 升級需要錢$'.(int)$this->config->get('VIP'.$this->checklevel($sender))[11].',請使用/vip up y確認' ) ;
					return true;
			}
		}
		if ( $args[0] == 'q' ) {
			if ( !isset($args[1]) ) goto cr;
			if ( $args[1]<1 OR $args[1]>$this->config->get('max') ) goto cr;
			$n = $this->config->get('VIP'.(int)$args[1]);
			$sender->sendMessage( '§c-- '.$this->config->get('VIP'.$args[1])['n'].' §c(VIP'.(int)$args[1].') --' );
			for ( $i=1,$j=1;$i<=12;$i++ ) {
				if ( isset($n[$i]) ) {
					$sender->sendMessage( '§b'.$j.'.'.$this->qh[$i] );
					$j++;
				}
			}
			if ( $this->config->get('sign')!=false )
				$sender->sendMessage( '§e'.$j.'.VIP每天簽到' );
			return true;
		}
		if ( $args[0] == 'buy' AND $sender instanceof Player ) {
			if ( $this->config->get('buy-VIP') != true ) goto cr;
			if ( !isset($args[2]) ) goto cr;
			$tp = (int) $args[1];
			if ( $tp<0 OR $tp>1 ) goto cr;
			$time = (int)$args[2];
			if ( empty($args[3]) ) $args[3] = '';
			if ( $time <= 0 ) {
				$sender->sendMessage ( '§c[VIP] 時間不正確' );
			}
			$mm = $this->getServer()->getPluginManager()->getPlugin("EconomyAPI")->myMoney($sender->getName());
			if ( $tp == 0 ) {
				$tm = $this->config->get('buy-1day') * $time ;
				if ( $tm > $mm ) {
					$sender->sendMessage('§c[VIP] 你的錢不夠!! 需要$'.$tm);
					return true;
				}
				if ( $args[3] != 'y' ) {
					$sender->sendMessage ( '§b[VIP] '.$time.'天VIP需要§c$'.$tm."\n".'§b[VIP] 請使用指令: §e/v buy '.$tp.' '.$time.' y §b確認購買/續費' );
					return true;
				}
				$this->getServer()->getPluginManager()->getPlugin("EconomyAPI")->reduceMoney($sender->getName(), $tm );
				if ( !$this->addtime( $sender , $time , 1 , 0 ) ) {
					$sender->sendMessage('§c[VIP] 類型不正確!!!');
					return true;
				}
				$sender->sendMessage('§a[VIP] 成功用$'.$tm.'購買/續費'.$time.'天VIP');
				return true;
			}
			elseif ( $tp == 1 ) {
				$tm = $this->config->get('buy-1hour') * $time ;
				if ( $tm > $mm ) {
					$sender->sendMessage('§c[VIP] 你的錢不夠!! 需要$'.$tm);
					return true;
				}
				if ( $args[3] != 'y' ) {
					$sender->sendMessage ( '§b[VIP] '.$time.'小時VIP需要§c$'.$tm."\n".'§b[VIP] 請使用指令: §e/v buy '.$tp.' '.$time.' y §b確認購買/續費' );
					return true;
				}
				if ( !$this->addtime( $sender , $time , 1 , 1 ) ) {
					$sender->sendMessage('§c[VIP] 類型不正確!!!');
					return true;
				}
				$this->getServer()->getPluginManager()->getPlugin("EconomyAPI")->reduceMoney($sender->getName(), $tm );
				$sender->sendMessage('§a[VIP] 成功用$'.$tm.'購買/續費'.$time.'小時VIP');
				return true;
			}
		}
		cr:
		$sender->sendMessage( '§c-----' );
		$sender->sendMessage( '§b-查看VIP權限: /vip q <VIP等級>' );
		if ( $this->config->get('buy-VIP') == true AND $sender instanceof Player ) {
			$sender->sendMessage( '§b-購買/續費VIP: /vip buy <類型(0/1)> <時間> [y]'."\n".'§c (類型0以天計算,類型1以小時計算,且類型1只計算「上線時間」)' );
		}
		if ( $sender instanceof Player AND $this->isvip($sender) ) {
			if ( $this->config->get('sign')!=false )
				$sender->sendMessage( '§e-VIP每天簽到: /vip sign' );
			$n = $this->config->get('VIP'.$this->checklevel($sender));
			for ( $i=0;$i<=12;$i++ ) {
				if ( isset($n[$i]) OR $i==0 )
					if ( $i!=10 )
						$sender->sendMessage( $this->cf[$i] );
			}
		}
		if ( $sender->isOp() ) {
			foreach ( $this->opcf as $v )
				$sender->sendMessage( $v );
		}
		$sender->sendMessage( '§c-----' );
		return true;
	}
	
	//   EVENT
	
	/*function tap ( PlayerInteractEvent $e ) {
		
	}
	
	function bb ( BlockBreakEvent $e ) {
		
	}
	
	function bp ( BlockPlaceEvent $e ) {
		
	}*/
	
	function onjoin ( PlayerJoinEvent $e ) {
		$pp = $e->getPlayer();
		$p = $pp->getName();
		if ( $this->isvip ($p) ){
			$w = $this->wvip($p);
			if ( $w == 'vip' ) {
				if ( strtotime($this->vip->get($this->ec($p))['et'])<time() ) {
					$this -> delvip ( $p ) ;
					return;
				}
				$i = $this->vip->get($this->ec($p));
				$pp->sendMessage ( '§e[VIP] 尊敬的'.$this->config->get('VIP'.$i['l'])['n'].'§e '.$pp->getName().',歡迎你回來!!'."\n".'§e[VIP] 你的VIP會在§b'.$i['et'].'§e到期' ) ;
			} else if ( $w == 'hvip' ) {
				if ( $this->hvip->get($this->ec($p))['et']<=0 ) {
					$this -> delvip ( $p ) ;
					return;
				}
				$this->j[$this->ec($p)] = time();
				$i = $this->hvip->get($this->ec($p));
				$pp->sendMessage ( '§e[VIP] 尊敬的'.$this->config->get('VIP'.$i['l'])['n'].'§e '.$pp->getName().',歡迎你回來!!'."\n".'§e[VIP] 你的VIP時間尚餘§b'.$this->ct($i['et']) ) ;
				if ( $i['et'] <= 300 )
					$pp->sendMessage('§c[VIP] 你的VIP時間尚餘幾分鐘過期，若要繼續維持VIP服務，請盡快續費!!!');
			}
		}
	}
	
	function quit ( PlayerQuitEvent $e ) {
		$p = $this->ec($e->getPlayer());
		if ( isset($this->j[$p]) ) {
			$t = time() - $this->j[$p];
			$this->hvip->setNested ( $p.'.et' , $this->hvip->get($p)['et']- ($t%61) ) ;
			$this->hvip->save();
			unset ( $this->j[$p] );
		}
	}
	
	//   VIP
	
	function ec ( $p ) {
		if ( $p instanceof Player or $p instanceof CommandSender ) $p = $p->getName();
		return strtolower ( $p ) ;
	}
	
	function wvip ( $p ) {
		$p = $this->ec($p);
		if ( $this -> vip -> exists ( $p ) )
			return 'vip';
		elseif ( $this -> hvip -> exists ( $p ) )
			return 'hvip';
		return 'false';
	}
	
	function isvip ( $p ) {
		$p = $this->ec($p);
		return $this -> vip -> exists ( $p ) OR $this -> hvip -> exists ( $p ) ;
	}
	
	function checklevel ( $p, $name = false ) {
		if ( ! $this->isvip($p) ) return false;
		$p = $this->ec($p);
		$w = $this->wvip($p);
		if ( $w == 'vip' )
			$l = $this -> vip -> get ( $p ) [ 'l' ] ;
		elseif ( $w == 'hvip' )
			$l = $this -> hvip -> get ( $p ) [ 'l' ] ;
		else
			return false ;
		if ( $name === true ) {
			$l = $this->config->get('VIP'.$l)['n'];
		}
		return $l;
	}
	
	function setlevel ( $p , $l ) {
		if ( ! $this->isvip($p) ) return false;
		$p = $this->ec($p);
		if ( $l<1 OR $l>$this->config->get('max') ) return false;
		$w = $this->wvip($p);
		if ( $w == 'vip' ) {
			$this->vip->setNested ( $p.'.l' , (int)$l ) ;
			$this -> vip -> save () ;
		}
		elseif ( $w == 'hvip' ) {
			$this->hvip->setNested ( $p.'.l' , (int)$l ) ;
			$this -> hvip -> save () ;
		}
		else
			return false;
	}
	
	function addtime ( $p , $time = 1 , $level = 1 , $tp = 0 ) {
		$p = $this->ec($p);
		if ( $tp == 0 ) {
			if ( !$this->isvip($p) ) {
				$this->vip->set( $p, [ 'l'=>$level,'st'=>date('Y/n/j H:i:s'),'et'=>date('Y/n/j H:i:s',time()+(86400*$time)) ] );
			} elseif ( $this->hvip->exists($p) )
				return false;
			else {
				$this->vip->setNested ( $p.'.et' , date('Y/n/j H:i:s',strtotime($this->vip->get($p)['et'])+(86400*$time)) ) ;
			}
			if ( strtotime($this->vip->get($p)['et'])<time() ) $this -> delvip ( $p ) ;
			$this -> vip -> save () ;
			return true;
		}
		elseif ( $tp == 1 ) {
			if ( !$this->isvip($p) ) {
				$this->hvip->set( $p, [ 'l'=>$level,'st'=>date('Y/n/j H:i:s'),'et'=>(3600*$time) ] );
			} elseif ( $this->vip->exists($p) )
				return false;
			else {
				$this->hvip->setNested ( $p.'.et' , $this->hvip->get($p)['et']+(3600*$time) ) ;
			}
			if ( $this->hvip->get($p)['et'] <= 0 ) $this -> delvip ( $p,2 ) ;
			$this -> hvip -> save () ;
			return true;
		}
		return false;
	}
	
	function deltime ( $p , $time = 1, $tp = 0 ) {
		$p = $this->ec($p);
		if ( !$this->isvip($p) ) return false;
		$w = $this->wvip($p);
		if ( $w == 'vip' AND $tp == 0 ) {
			$this->vip->setNested ( $p.'.et' , date('Y/n/j H:i:s',strtotime($this->vip->get($p)['et'])-(86400*$time)) ) ;
			if ( strtotime($this->vip->get($p)['et'])<time() ) $this -> delvip ( $p ) ;
			$this -> vip -> save () ;
			return true;
		}
		elseif ( $w == 'hvip' AND $tp == 1 ) {
			$this->hvip->setNested ( $p.'.et' , $this->hvip->get($p)['et']-(3600*$time) ) ;
			if ( $this->hvip->get($p)['et'] <= 0 ) $this -> delvip ( $p,1 ) ;
			$this -> hvip -> save () ;
			return true;
		}
		else return false;
	}
	
	function delvip ( $p , $tp = 3 ) {
		$p = $this->ec($p);
		if ( $this->vip->exists($p) AND $tp != 2 ) {
			$this->vip->remove($p);
			$this -> vip -> save () ;
		}
		if ( $this->hvip->exists($p) AND $tp != 1 ) {
			$this->hvip->remove($p);
			$this -> hvip -> save () ;
		}
		return true;
	}
	
	function isset10 ( $p ) {
		$p = $this->ec($p);
		if ( !$this->isvip($p) ) return false;
		return isset ( $this->config->get('VIP'.$this->checklevel($p))[10] ) ;
	}
	
	function ct ( $time , $tp = 'c' ) {
		$y = floor($time/32140800);
		$m = floor(($time%32140800)/2678400);
		$d = floor(($time%2678400)/86400);
		$h = floor(($time%86400)/3600);
		$ms = floor(($time%3600)/60);
		$s = floor($time%60);
		if ( $tp = 'c' ) {
			return ($y>0?$y.'年':'').($m>0?$m.'月':'').($d>0?$d.'日':'').($h>0?$h.'小時':'').($ms>0?$ms.'分鐘':'').$s.'秒';
		} else {
			return ($y>0?$y.'Y ':'').($m>0?$m.'M ':'').($d>0?$d.'D ':'').($h>0?$h.'h ':'').($ms>0?$ms.'m ':'').$s.'s';
		}
	}
	
	function viptime () {
		$time = time();
		foreach ( $this->getServer()->getOnlinePlayers() as $pl ) {
			if ( !$pl ) continue;
			$p = $this->ec($pl);
			if ( !$this->isvip($p) ) continue;
			if ( $this->hvip->exists($p) ) {
				if ( !empty($this->j[$p]) ) {
					$t = $time - $this->j[$p];
					$this->hvip->setNested ( $p.'.et' , $this->hvip->get($p)['et']- ($t%61) ) ;
				}
				$this->j[$p] = $time;
				$vt = $this->hvip->get($p)['et'];
				if ( $vt <= 300 ) {
					if ( $vt <= 0 ) {
						$pl->sendMessage('§c[VIP] 你的VIP已經過期!!歡迎再次購買!');
						$this->delvip($p);
						unset ($this->j[$p]) ;
					} else {
						$pl->sendMessage('§c[VIP] 你的VIP時間尚餘幾分鐘過期，若要繼續維持VIP服務，請盡快續費!!!');
					}
				}
			}
		}
		$this->hvip->save();
	}
	
}

use pocketmine\event\player\PlayerEvent;
use pocketmine\event\Cancellable;

class VIPtpEvent extends PlayerEvent implements Cancellable{
	
	public static $handlerList = null;
	
	public function __construct ( Player $p ) {
		$this->player = $p;
	}
	
}

class CallbackTask extends \pocketmine\scheduler\Task{

	/** @var callable */
	protected $callable;

	/** @var array */
	protected $args;

	/**
	 * @param callable $callable
	 * @param array    $args
	 */
	public function __construct(callable $callable, array $args = []){
		$this->callable = $callable;
		$this->args = $args;
		$this->args[] = $this;
	}

	/**
	 * @return callable
	 */
	public function getCallable(){
		return $this->callable;
	}

	public function onRun($currentTicks){
		call_user_func_array($this->callable, $this->args);
	}

}